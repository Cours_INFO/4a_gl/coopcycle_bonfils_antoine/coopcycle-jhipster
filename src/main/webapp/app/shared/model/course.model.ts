import { IUtilisateur } from '@/shared/model/utilisateur.model';

export interface ICourse {
  id?: number;
  utilisateurs?: IUtilisateur[] | null;
}

export class Course implements ICourse {
  constructor(public id?: number, public utilisateurs?: IUtilisateur[] | null) {}
}
