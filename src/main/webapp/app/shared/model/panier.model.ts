import { IProduit } from '@/shared/model/produit.model';

import { Role } from '@/shared/model/enumerations/role.model';
export interface IPanier {
  id?: number;
  utilisateur?: Role;
  produits?: IProduit[] | null;
}

export class Panier implements IPanier {
  constructor(public id?: number, public utilisateur?: Role, public produits?: IProduit[] | null) {}
}
