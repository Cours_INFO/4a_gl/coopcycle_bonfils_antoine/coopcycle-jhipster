import { IProduit } from '@/shared/model/produit.model';

export interface IBoutique {
  id?: number;
  idBoutique?: number;
  produit?: IProduit | null;
}

export class Boutique implements IBoutique {
  constructor(public id?: number, public idBoutique?: number, public produit?: IProduit | null) {}
}
