export enum Role {
  Client = 'Client',

  Societaire = 'Societaire',

  Directeur_General = 'Directeur_General',

  Commercant = 'Commercant',

  Coursier = 'Coursier',

  Banque = 'Banque',

  Fournisseur = 'Fournisseur',

  Administrateur = 'Administrateur',
}
