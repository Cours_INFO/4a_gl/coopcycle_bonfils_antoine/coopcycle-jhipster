import { Role } from '@/shared/model/enumerations/role.model';
export interface ICooperative {
  id?: number;
  directeur?: Role;
}

export class Cooperative implements ICooperative {
  constructor(public id?: number, public directeur?: Role) {}
}
