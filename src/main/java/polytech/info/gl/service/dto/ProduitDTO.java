package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link polytech.info.gl.domain.Produit} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ProduitDTO implements Serializable {

    private Long id;

    @NotNull
    private Long idProduit;

    @NotNull
    @Size(min = 2)
    @Pattern(regexp = "^[A-Z][a-z]+\\d$")
    private String nomProduit;

    @NotNull
    private Long prix;

    private Set<PanierDTO> estComposeDes = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public Long getPrix() {
        return prix;
    }

    public void setPrix(Long prix) {
        this.prix = prix;
    }

    public Set<PanierDTO> getEstComposeDes() {
        return estComposeDes;
    }

    public void setEstComposeDes(Set<PanierDTO> estComposeDes) {
        this.estComposeDes = estComposeDes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProduitDTO)) {
            return false;
        }

        ProduitDTO produitDTO = (ProduitDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, produitDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProduitDTO{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", nomProduit='" + getNomProduit() + "'" +
            ", prix=" + getPrix() +
            ", estComposeDes=" + getEstComposeDes() +
            "}";
    }
}
