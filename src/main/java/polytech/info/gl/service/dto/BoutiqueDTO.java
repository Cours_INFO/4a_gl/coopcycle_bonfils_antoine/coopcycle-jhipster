package polytech.info.gl.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link polytech.info.gl.domain.Boutique} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class BoutiqueDTO implements Serializable {

    private Long id;

    @NotNull
    private Long idBoutique;

    private ProduitDTO produit;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdBoutique() {
        return idBoutique;
    }

    public void setIdBoutique(Long idBoutique) {
        this.idBoutique = idBoutique;
    }

    public ProduitDTO getProduit() {
        return produit;
    }

    public void setProduit(ProduitDTO produit) {
        this.produit = produit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BoutiqueDTO)) {
            return false;
        }

        BoutiqueDTO boutiqueDTO = (BoutiqueDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, boutiqueDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BoutiqueDTO{" +
            "id=" + getId() +
            ", idBoutique=" + getIdBoutique() +
            ", produit=" + getProduit() +
            "}";
    }
}
