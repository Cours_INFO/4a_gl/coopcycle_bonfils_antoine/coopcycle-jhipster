package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Course;
import polytech.info.gl.service.dto.CourseDTO;

/**
 * Mapper for the entity {@link Course} and its DTO {@link CourseDTO}.
 */
@Mapper(componentModel = "spring")
public interface CourseMapper extends EntityMapper<CourseDTO, Course> {}
