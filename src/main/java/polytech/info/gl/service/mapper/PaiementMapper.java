package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Paiement;
import polytech.info.gl.service.dto.PaiementDTO;

/**
 * Mapper for the entity {@link Paiement} and its DTO {@link PaiementDTO}.
 */
@Mapper(componentModel = "spring")
public interface PaiementMapper extends EntityMapper<PaiementDTO, Paiement> {}
