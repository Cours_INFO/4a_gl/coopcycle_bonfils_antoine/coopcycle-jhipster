package polytech.info.gl.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import polytech.info.gl.domain.Panier;
import polytech.info.gl.domain.Produit;
import polytech.info.gl.service.dto.PanierDTO;
import polytech.info.gl.service.dto.ProduitDTO;

/**
 * Mapper for the entity {@link Produit} and its DTO {@link ProduitDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProduitMapper extends EntityMapper<ProduitDTO, Produit> {
    @Mapping(target = "estComposeDes", source = "estComposeDes", qualifiedByName = "panierIdSet")
    ProduitDTO toDto(Produit s);

    @Mapping(target = "removeEstComposeDe", ignore = true)
    Produit toEntity(ProduitDTO produitDTO);

    @Named("panierId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PanierDTO toDtoPanierId(Panier panier);

    @Named("panierIdSet")
    default Set<PanierDTO> toDtoPanierIdSet(Set<Panier> panier) {
        return panier.stream().map(this::toDtoPanierId).collect(Collectors.toSet());
    }
}
