package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Course;
import polytech.info.gl.domain.Utilisateur;
import polytech.info.gl.service.dto.CourseDTO;
import polytech.info.gl.service.dto.UtilisateurDTO;

/**
 * Mapper for the entity {@link Utilisateur} and its DTO {@link UtilisateurDTO}.
 */
@Mapper(componentModel = "spring")
public interface UtilisateurMapper extends EntityMapper<UtilisateurDTO, Utilisateur> {
    @Mapping(target = "course", source = "course", qualifiedByName = "courseId")
    UtilisateurDTO toDto(Utilisateur s);

    @Named("courseId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourseDTO toDtoCourseId(Course course);
}
