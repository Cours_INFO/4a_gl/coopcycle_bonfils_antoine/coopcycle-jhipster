package polytech.info.gl.service.mapper;

import org.mapstruct.*;
import polytech.info.gl.domain.Boutique;
import polytech.info.gl.domain.Produit;
import polytech.info.gl.service.dto.BoutiqueDTO;
import polytech.info.gl.service.dto.ProduitDTO;

/**
 * Mapper for the entity {@link Boutique} and its DTO {@link BoutiqueDTO}.
 */
@Mapper(componentModel = "spring")
public interface BoutiqueMapper extends EntityMapper<BoutiqueDTO, Boutique> {
    @Mapping(target = "produit", source = "produit", qualifiedByName = "produitId")
    BoutiqueDTO toDto(Boutique s);

    @Named("produitId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProduitDTO toDtoProduitId(Produit produit);
}
