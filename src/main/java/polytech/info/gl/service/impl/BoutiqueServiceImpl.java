package polytech.info.gl.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polytech.info.gl.domain.Boutique;
import polytech.info.gl.repository.BoutiqueRepository;
import polytech.info.gl.service.BoutiqueService;
import polytech.info.gl.service.dto.BoutiqueDTO;
import polytech.info.gl.service.mapper.BoutiqueMapper;

/**
 * Service Implementation for managing {@link Boutique}.
 */
@Service
@Transactional
public class BoutiqueServiceImpl implements BoutiqueService {

    private final Logger log = LoggerFactory.getLogger(BoutiqueServiceImpl.class);

    private final BoutiqueRepository boutiqueRepository;

    private final BoutiqueMapper boutiqueMapper;

    public BoutiqueServiceImpl(BoutiqueRepository boutiqueRepository, BoutiqueMapper boutiqueMapper) {
        this.boutiqueRepository = boutiqueRepository;
        this.boutiqueMapper = boutiqueMapper;
    }

    @Override
    public BoutiqueDTO save(BoutiqueDTO boutiqueDTO) {
        log.debug("Request to save Boutique : {}", boutiqueDTO);
        Boutique boutique = boutiqueMapper.toEntity(boutiqueDTO);
        boutique = boutiqueRepository.save(boutique);
        return boutiqueMapper.toDto(boutique);
    }

    @Override
    public BoutiqueDTO update(BoutiqueDTO boutiqueDTO) {
        log.debug("Request to update Boutique : {}", boutiqueDTO);
        Boutique boutique = boutiqueMapper.toEntity(boutiqueDTO);
        boutique = boutiqueRepository.save(boutique);
        return boutiqueMapper.toDto(boutique);
    }

    @Override
    public Optional<BoutiqueDTO> partialUpdate(BoutiqueDTO boutiqueDTO) {
        log.debug("Request to partially update Boutique : {}", boutiqueDTO);

        return boutiqueRepository
            .findById(boutiqueDTO.getId())
            .map(existingBoutique -> {
                boutiqueMapper.partialUpdate(existingBoutique, boutiqueDTO);

                return existingBoutique;
            })
            .map(boutiqueRepository::save)
            .map(boutiqueMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BoutiqueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Boutiques");
        return boutiqueRepository.findAll(pageable).map(boutiqueMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BoutiqueDTO> findOne(Long id) {
        log.debug("Request to get Boutique : {}", id);
        return boutiqueRepository.findById(id).map(boutiqueMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Boutique : {}", id);
        boutiqueRepository.deleteById(id);
    }
}
